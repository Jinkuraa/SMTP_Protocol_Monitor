#!/bin/bash

if [ -z "$1" ]
  then
    echo "Veuillez passer le nom d'un fichier pcap smtp"
else
    mvn -q clean compile exec:java -Dexec.args="$1" 2> /dev/null
fi
