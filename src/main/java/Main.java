

public class Main {

    private static void handleArgs(String [] args) throws Exception {
        if(args.length == 0){
            System.out.println("No file was provided.");
        } else {
            System.out.println("SMTP Fields : ");
            PcapReader reader = new PcapReader(args[0]);
            ParserSMTP parser = new ParserSMTP(reader.getDecryptedPcap());
            parser.printFields();
            System.out.println("\nVerdicts : ");
            PropertyAnalyzer analyzer = new PropertyAnalyzer(reader.getDecryptedPcap());
            analyzer.applyProperties();
        }
    }
    public static void main(String[] args) throws Exception {
        handleArgs(args);
    }
}
