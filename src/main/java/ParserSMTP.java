import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ParserSMTP {
    private String decryptedPcap;
    private static final Pattern pattern = Pattern.compile("^(\\d{3}) (.*)", Pattern.CASE_INSENSITIVE);
    private static final Pattern pattern_multiline = Pattern.compile("^(\\d{3})-(.*)", Pattern.CASE_INSENSITIVE);

    public ParserSMTP (String s){
        decryptedPcap = s;
    }

    private String handleMultilineCodeResponse(Scanner scan, String beginning)  {
        StringBuilder builder = new StringBuilder();
        StringBuilder parameters_builder = new StringBuilder();
        while( scan.hasNextLine() )
        {
            String line = scan.nextLine();
            Matcher matcher = pattern.matcher(line);
            Matcher matcher_multiline = pattern_multiline.matcher(line);
            if(matcher_multiline.matches()){
                parameters_builder.append(matcher_multiline.group(2)+" ");
            }
            if(matcher.find()){
                parameters_builder.append(matcher.group(2));
                builder.append("CODE: "+matcher.group(1)+" PARAMETERS: "+beginning+" "+matcher.group(2)+" "+parameters_builder.toString());
                return builder.toString();
            }
        }
        return "END OF MULTILINE REQUEST NOT FOUND";

    }

    private String decomposeCommandWithParameters(String command, String line){
        int commandLength = command.length();
        return "COMMAND: "+line.substring(0,commandLength)+ " PARAMETER(S) : "+line.substring(commandLength);
    }

    private String extractCommand(String command , String line){
        return line.substring(0,command.length());
    }
    private String handleCommands(String line){

        if(line.startsWith("EHLO ")||line.startsWith("HELO ")) {
            return decomposeCommandWithParameters(extractCommand("EHLO ",line),line);
        }
        if(line.startsWith("MAIL FROM: ")){
            return decomposeCommandWithParameters(extractCommand("MAIL FROM: ",line),line);
        }
        if(line.startsWith("RCPT TO: ")){
            return decomposeCommandWithParameters(extractCommand("RCPT TO: ",line),line);
        }
        if(line.equals("QUIT")){
            return "COMMAND: QUIT";
        }

        if(line.equals("DATA")){
            return "COMMAND: DATA";
        }
        return "";
    }


    private void printReponse(Matcher matcher){
        System.out.println("CODE : "+matcher.group(1)+ " PARAMETER : "+matcher.group(2));
    }

    private String buildMail(Scanner scan){
        StringBuilder mailBuilder = new StringBuilder();
        while(scan.hasNextLine()) {
            String line = scan.nextLine();
            if(line.length() == 1 &&line.charAt(0) == '.')  return mailBuilder.toString();
            mailBuilder.append(line+"\n");
        }
        return "";
    }

    private void handleMailContent(String line, Scanner scan){
        if(line.equals("DATA")){
            if(scan.hasNextLine()){
                line = scan.nextLine();
                Matcher matcher = pattern.matcher(line);
                if(matcher.matches()){
                    printReponse(matcher);
                    if(matcher.group(1).equals("354")){
                        String mail = buildMail(scan);
                        System.out.println("MAIL HEADER : ");
                        try {
                            printMailFields(mail);
                        } catch (MessagingException e) {
                            System.out.println("MESSAGING EXCEPTION !");
                        }
                    }
                }
            }
        }
    }

    private void printMailFields (String mailContent) throws MessagingException {
        Session s = Session.getInstance(new Properties());
        InputStream is = new ByteArrayInputStream(mailContent.getBytes());
        MimeMessage message = new MimeMessage(s, is);
        message.getAllHeaderLines();
        System.out.println("(");
        for (Enumeration<Header> e = message.getAllHeaders(); e.hasMoreElements();) {
            Header h = e.nextElement();
            System.out.println(h.getName()+": "+ h.getValue());
        }
        System.out.println(")");
    }

    public void printFields(){
        Scanner scan = new Scanner(decryptedPcap);
        while( scan.hasNextLine() )
        {
            String line = scan.nextLine();
            Matcher matcher = pattern.matcher(line);
            Pattern pattern_multiline = Pattern.compile("^(\\d{3})-(.*)", Pattern.CASE_INSENSITIVE);
            Matcher matcher_multiline = pattern_multiline.matcher(line);
            if(matcher_multiline.matches()){
               System.out.println(handleMultilineCodeResponse(scan,matcher_multiline.group(2)));
            }
            if(matcher.matches()){
               printReponse(matcher);
            }
            if(!matcher.matches() && !matcher_multiline.matches()){
                System.out.println(handleCommands(line));
                handleMailContent(line,scan);
            }
        }
    }

}
