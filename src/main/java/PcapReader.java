import io.pkts.PacketHandler;
import io.pkts.Pcap;
import io.pkts.buffer.Buffer;
import io.pkts.packet.Packet;
import io.pkts.packet.TCPPacket;
import io.pkts.packet.UDPPacket;
import io.pkts.protocol.Protocol;

import java.io.IOException;

public class PcapReader {
    private String decryptedPcap;

    public PcapReader(String filePath) throws IOException {
        final StringBuilder b = new StringBuilder();
        final Pcap pcap = Pcap.openStream(filePath);
        pcap.loop(new PacketHandler() {
            public boolean nextPacket(Packet packet) throws IOException {
                if (packet.hasProtocol(Protocol.TCP)) {
                    TCPPacket tcpPacket = (TCPPacket) packet.getPacket(Protocol.TCP);
                    Buffer buffer = tcpPacket.getPayload();
                    if (buffer != null) {
                        b.append(buffer.toString());
                    }
                } else if (packet.hasProtocol(Protocol.UDP)) {
                    UDPPacket udpPacket = (UDPPacket) packet.getPacket(Protocol.UDP);
                    Buffer buffer = udpPacket.getPayload();
                    if (buffer != null) {
                        b.append(buffer.toString());
                    }
                }
                return true;
            }
        });
        decryptedPcap = b.toString();
    }

    public String getDecryptedPcap() {
        return decryptedPcap;
    }
}
