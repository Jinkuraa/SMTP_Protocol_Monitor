
import java.util.Scanner;


public class PropertyAnalyzer {

    final private String decryptedPcap;
    private int alertCount = 0;
    final private static int MAX_ALERT_COUNT = 5;

    public PropertyAnalyzer(String decryptedPcap){
        this.decryptedPcap = decryptedPcap;
    }

    public boolean existingCode(String code) {
        return (code.equals("220")) || (code.equals("250")) || (code.equals("354")) ||
                (code.equals("421")) || (code.equals("452")) || (code.equals("550")) ||
                (code.equals("554"));
    }

    public boolean propertyDot() {
        Scanner scan = new Scanner(decryptedPcap);
        String l;
        while(scan.hasNextLine()) {
            l = scan.nextLine();
            if((l.length()>=3) && l.substring(0,3).equals("354")) {
                while(l.charAt(0) == '.' & scan.hasNextLine()) {
                    l = scan.nextLine();
                    if(existingCode(l.substring(0,3)) || !scan.hasNextLine()) return false;
                }
            }
        }
        scan.close();
        return true;
    }

    public boolean propertyConnexion()  {
        Scanner scan = new Scanner(decryptedPcap);
        String l;
        while(scan.hasNextLine()) {
            l = scan.nextLine();
            if( (l.length()>=3) && existingCode(l.substring(0,3))){
                if(!l.substring(0,3).equals("220") && !l.substring(0,3).equals("554")) return false;
                return true;
            }
        }
        return true;
    }

    private void checkBruteForce(){
        Scanner scan = new Scanner(decryptedPcap);
        while( scan.hasNextLine() )
        {
            String line = scan.nextLine();
            if(line.startsWith("RCPT TO:")){
                if(!scan.hasNextLine()) return;
                line = scan.nextLine();
                if(!(line.length()<3)) {
                    if (line.substring(0, 3).equals("550")) alertCount++;
                }
            }
        }
    }

    private boolean alertCountCheck(){
        alertCount = 0;
        checkBruteForce();
        return alertCount >= MAX_ALERT_COUNT;
    }

    public static boolean isBase64(String s) {
        String a = "[a-zA-Z0-9]*={0,2}";
        return s.matches(a);
    }

    public static boolean isAuthResultValid(String authenticationResult) {
        String[] split = authenticationResult.split(" ");
        if (split.length > 3)
            return false;
        boolean authentication = split[1].equals("Authentication");
        String verdict = split[2];
        if (split[0].equals("235"))
            return authentication && verdict.equals("succeeded");
        else if (split[0].equals("535"))
            return authentication && verdict.equals("failed");
        else
            return false;
    }

    public static boolean isChallengeValid(String challengeLine) {
        String[] split = challengeLine.split(" ");
        String authCode = split[0];
        String base64reply = split[1];
        return authCode.equals("334") && isBase64(base64reply);
    }
    private int newIndex(int i, String[] result){
        for(int currentIndex = i; currentIndex < result.length; currentIndex++){
            if(result[currentIndex].startsWith("AUTH PLAIN")||result[currentIndex].equals("AUTH LOGIN")||result[currentIndex].equals("AUTH CRAM-MD5")) return currentIndex;
        }
        return -1;
    }

    public boolean authProperty() {
        String s = decryptedPcap;
        String[] result = s.split("[\\r\\n]+");
        for (int i = 0; i < result.length; i++) {
            if (result[i].startsWith("250-AUTH PLAIN LOGIN") || result[i].startsWith("250 AUTH PLAIN LOGIN")) {
                int newIndex  = newIndex(i,result);
                if(newIndex == -1) {
                    System.out.println("DOIAHZODIHAZO");
                    return false;
                }
                String authMechanism = result[newIndex];
                switch (authMechanism) {
                    case "AUTH PLAIN":
                        String mechanismConfirmation = result[newIndex + 1];
                        String credentials = result[newIndex + 2];
                        String authResultAP = result[newIndex + 3];
                        return mechanismConfirmation.equals("334") && isBase64(credentials)
                                && isAuthResultValid(authResultAP);
                    case "AUTH LOGIN":
                        String loginQuestion = result[newIndex + 1];
                        String login = result[newIndex + 2];
                        String passwordQuestion = result[newIndex + 3];
                        String password = result[newIndex + 4];
                        String authResultAL = result[newIndex + 5];
                        return loginQuestion.equals("334 VXNlcm5hbWU6") && isBase64(login)
                                && passwordQuestion.equals("334 UGFzc3dvcmQ6") && isBase64(password)
                                && isAuthResultValid(authResultAL);
                    case "AUTH CRAM-MD5":
                        String challengeLine = result[newIndex + 2];
                        String challengeAnswerLine = result[newIndex + 3];
                        String authResultACM = result[newIndex + 4];
                        return isChallengeValid(challengeLine) && isBase64(challengeAnswerLine)
                                && isAuthResultValid(authResultACM);
                    default:
                        String[] split = result[newIndex + 2].split(" ");
                        String base64credentials = split[2];
                        if (split.length > 3)
                            return false;
                        return split[0].equals("AUTH") && split[1].equals("PLAIN") && isBase64(base64credentials);
                }
            }
        }
        return false;
    }

    public void applyProperties() throws Exception {
        if(propertyConnexion()) System.out.println("Connexion property is valid.");
        else System.out.println("Connexion property is invalid.");
        if(propertyDot()) System.out.println("Dot property is valid.");
        else System.out.println("Dot property is invalid.");
        if(alertCountCheck())System.out.println("The user is bruteforcing.");
        else System.out.println("The user is not bruteforcing.");
        if(authProperty())System.out.println("Auth has succeeded.");
        else System.out.println("Auth has failed or there is no Auth.");
    }






}
